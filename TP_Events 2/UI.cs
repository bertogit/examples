﻿using System;
using System.Collections.Generic;
using Crestron.SimplSharp;                          	// For Basic SIMPL# Classes
using Crestron.SimplSharpPro;                       	// For Basic SIMPL#Pro classes
using Crestron.SimplSharpPro.CrestronThread;        	// For Threading
using Crestron.SimplSharpPro.Diagnostics;		    	// For System Monitor Access
using Crestron.SimplSharpPro.DeviceSupport;         	// For Generic Device Support
using Crestron.SimplSharpPro.UI;
using Crestron.SimplSharp.CrestronIO;

namespace TP_Events
{
    enum Joins
    {
        dig101 = 101,
        dig102 = 102,
        dig103 = 103,
        dig104 = 104,
        ana101 = 101,
        ser101 = 101,
    };

    public class UI
    {
        public uint ID;
        public CrestronControlSystem CS;
        private BasicTriListWithSmartObject TP;

        public UI(uint ID, CrestronControlSystem CS)
        {
            this.ID = ID;
            this.CS = CS;
        }

        public void Initialize()
        {
            if (CS.SupportsEthernet)
            {
                TP = new XpanelForSmartGraphics(ID, CS);
                if (TP.Register() != eDeviceRegistrationUnRegistrationResponse.Success)
                    ErrorLog.Error("Panel {0} did not register: {1}", TP.ID, TP.RegistrationFailureReason);
                else
                {
                    String SGDpath = string.Format(@"{0}\User\TP_Events.sgd", Directory.GetApplicationRootDirectory());
                    if (File.Exists(SGDpath))
                    {
                        TP.LoadSmartObjects(SGDpath);
                        CrestronConsole.PrintLine("loaded {0} smartobjects", TP.SmartObjects.Count);
                    }
                    TP.SigChange += new SigEventHandler(TP_SigChange);
                    TP.SmartObjects[1].SigChange += new SmartObjectSigChangeEventHandler(TP_SO_SigChange);
                    RegisterJoins();
                }           
            }
        }

        private void RegisterJoins()
        {
            TP.BooleanOutput[(uint)Joins.dig101].UserObject = new Action<bool>(dig101);
            TP.UShortOutput[(uint)Joins.ana101].UserObject = new Action<ushort>(ana101);
            TP.StringOutput[(uint)Joins.ser101].UserObject = new Action<string>(ser101);
        }

        void TP_SigChange(BasicTriList currentDevice, SigEventArgs args)
        {
            //CrestronConsole.PrintLine("> Event Type: {0}, Signal: {1}", args.Sig.Type, args.Sig.Name);
            switch (args.Sig.Type)
            {
                case eSigType.Bool:
                    (args.Sig.UserObject as Action<bool>)(args.Sig.BoolValue);
                    break;
                case eSigType.UShort:
                    (args.Sig.UserObject as Action<ushort>)(args.Sig.UShortValue);
                    break;
                case eSigType.String:
                    (args.Sig.UserObject as Action<string>)(args.Sig.StringValue);
                    break;
            }
        }

        void TP_SO_SigChange(GenericBase currentDevice, SmartObjectEventArgs args)
        {
            //CrestronConsole.PrintLine("> SO Event Type: {0}, Signal: {1}", args.Sig.Type, args.Sig.Name);
            switch (args.Sig.Type)
            {
                case eSigType.Bool:
                    (args.Sig.UserObject as Action<bool>)(args.Sig.BoolValue);
                    break;
                case eSigType.UShort:
                    (args.Sig.UserObject as Action<ushort>)(args.Sig.UShortValue);
                    break;
                case eSigType.String:
                    (args.Sig.UserObject as Action<string>)(args.Sig.StringValue);
                    break;
            }
        }

        #region DIGITALS
        internal void dig101(bool dig)
        {
            if (dig)
                CrestronConsole.PrintLine("> dig101 pushed");
            else
                CrestronConsole.PrintLine("> dig101 released");
        }
        #endregion

        #region ANALOGS
        internal void ana101(ushort ana)
        {
            CrestronConsole.PrintLine("> ana101 = {0}", ana);
            TP.UShortInput[(uint)Joins.ana101].UShortValue = ana;
        }
        #endregion

        #region SERIALS
        internal void ser101(string ser)
        {
            CrestronConsole.PrintLine("> ser101 = {0}", ser);
        }
        #endregion
    }
}