﻿using System;
using Crestron.SimplSharp;
using Crestron.SimplSharpPro;
using Crestron.SimplSharpPro.DeviceSupport;         	// For Generic Device Support

namespace TP_Events
{
    /* Example 1)
     *      private ButtonHeld hold;
     *      if (args.Sig.BoolValue) 
     *          hold = new ButtonHeld(4000, 300, currentDevice, args, BackspaceHeld);
     *      else if (!args.Sig.BoolValue)
     *          hold.Stop();
     *          
     * Example 2)
     *      private ButtonHeld hold = new ButtonHeld();
     *          if (args.Sig.BoolValue) 
     *          {
                    hold.Start(3000, 300, currentDevice, args);
     *              hold.HeldEvent += new SigEventHandler(BackspaceHeld);
     *          }
     *          else if (!args.Sig.BoolValue)
     *          {
     *              hold.Stop();
     *              hold.HeldEvent -= new SigEventHandler(BackspaceHeld);
     *          }
    */

    public class ButtonHeld
    {
        private CTimer cTimer;
        private CTimerCallbackFunction cTimerCallbackFunction;
        private BasicTriList currentDevice;
        private SigEventArgs heldArgs;
        private SigEventHandler callback;
        public event SigEventHandler HeldEvent;
        private int defaultHold = 2000;
        private long defaultRepeat = 200;
        public int holdTime;            // button hold time in 1/1000 seconds
        public long repeatTime;         // hold repeat time in 1/1000 seconds

        public ButtonHeld()
        {
            this.holdTime = defaultHold;
            this.repeatTime = defaultRepeat;
            cTimerCallbackFunction = new CTimerCallbackFunction(callbackInvokeFunction);
            cTimer = new CTimer(cTimerCallbackFunction, 0);
        }
        public ButtonHeld(int holdTime, long repeatTime)
        {
            this.holdTime = holdTime;
            this.repeatTime = repeatTime;
            cTimerCallbackFunction = new CTimerCallbackFunction(callbackInvokeFunction);
            cTimer = new CTimer(cTimerCallbackFunction, 0);
        }
        public ButtonHeld(int holdTime, long repeatTime, BasicTriList currentDevice, SigEventArgs args, SigEventHandler callback)
        {
            cTimerCallbackFunction = new CTimerCallbackFunction(callbackStandardFunction);
            cTimer = new CTimer(cTimerCallbackFunction, 0);
            this.currentDevice = currentDevice;
            this.heldArgs = args;
            this.callback = callback;
            this.holdTime = holdTime;
            this.repeatTime = repeatTime;
            cTimer.Reset(holdTime, repeatTime);
        }

        private void callbackStandardFunction(Object obj)
        {
            if (heldArgs != null && currentDevice != null)
                callback(currentDevice, heldArgs);
        }
        private void callbackInvokeFunction(Object obj)
        {
            if (heldArgs != null && currentDevice != null)
                HeldEvent.Invoke(currentDevice, heldArgs);
        }
        public void Start(int holdTime, long repeatTime, BasicTriList currentDevice, SigEventArgs args)
        {
            this.holdTime = holdTime;
            this.repeatTime = repeatTime;
            this.currentDevice = currentDevice;
            this.heldArgs = args;
            cTimer.Reset(holdTime, repeatTime);
        }
        public void Stop()
        {
            cTimer.Stop();
            holdTime = defaultHold;
            repeatTime = defaultRepeat;
            currentDevice = null;
            heldArgs = null;
            callback = null;
        }
    }
}