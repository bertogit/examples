﻿using System;
using System.Collections.Generic;
using Crestron.SimplSharp;
using Crestron.SimplSharpPro;
using Crestron.SimplSharpPro.DeviceSupport;         	// For Generic Device Support
using Crestron.SimplSharpPro.UI;

namespace TP_Events
{
    public partial class ControlSystem
    {
        enum Joins
        {
            dig101 = 101,
            dig102 = 102,
            dig103 = 103,
            dig104 = 104,
            ana101 = 101,
            ser101 = 101,
        };

        private Dictionary<uint, SigEventHandler> digitals;
        private Dictionary<uint, SigEventHandler> analogs;
        private Dictionary<uint, SigEventHandler> serials;

        public void InitializeSignals()
        {
            #region JOIN_DICTIONARY
            digitals = new Dictionary<uint, SigEventHandler>()
            {
                {(uint)Joins.dig101, dig101},
                {(uint)Joins.dig102, dig102},
                {(uint)Joins.dig103, dig103},
                {(uint)Joins.dig104, dig104},
            };
            analogs = new Dictionary<uint, SigEventHandler>()
            {
                {(uint)Joins.ana101, ana101},
            };
            serials = new Dictionary<uint, SigEventHandler>()
            {
                {(uint)Joins.ser101, ser101},
            };
            #endregion
        }

        #region DIGITALS
        private void dig101(BasicTriList currentDevice, SigEventArgs args)
        {
            if (args.Sig.BoolValue)
                CrestronConsole.PrintLine("{0}: pushed", args.Sig.Name);
            else
                CrestronConsole.PrintLine("{0}: released", args.Sig.Name);
        }
        private void dig102(BasicTriList currentDevice, SigEventArgs args)
        {
            if (args.Sig.BoolValue)
                CrestronConsole.PrintLine("{0}: pushed", args.Sig.Name);
            else
                CrestronConsole.PrintLine("{0}: released", args.Sig.Name);
        }
        private void dig103(BasicTriList currentDevice, SigEventArgs args)
        {
            if (args.Sig.BoolValue)
                CrestronConsole.PrintLine("{0}: pushed", args.Sig.Name);
            else
                CrestronConsole.PrintLine("{0}: released", args.Sig.Name);
        }
        private void dig104(BasicTriList currentDevice, SigEventArgs args)
        {
            if (args.Sig.BoolValue)
                CrestronConsole.PrintLine("{0}: pushed", args.Sig.Name);
            else
                CrestronConsole.PrintLine("{0}: released", args.Sig.Name);
        }
        #endregion

        #region ANALOGS
        private void ana101(BasicTriList currentDevice, SigEventArgs args)
        {
            CrestronConsole.PrintLine("{0}: {1}", args.Sig.Name, args.Sig.UShortValue);
            currentDevice.UShortInput[args.Sig.Number].UShortValue = args.Sig.UShortValue;
        }
        #endregion

        #region SERIALS
        private void ser101(BasicTriList currentDevice, SigEventArgs args)
        {
            CrestronConsole.PrintLine("{0}: {1}", args.Sig.Name, args.Sig.StringValue);
        }
        #endregion

    }
}