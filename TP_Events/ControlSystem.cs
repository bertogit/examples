using System;
using System.Collections.Generic;
using Crestron.SimplSharp;                          	// For Basic SIMPL# Classes
using Crestron.SimplSharpPro;                       	// For Basic SIMPL#Pro classes
using Crestron.SimplSharpPro.CrestronThread;        	// For Threading
using Crestron.SimplSharpPro.Diagnostics;		    	// For System Monitor Access
using Crestron.SimplSharpPro.DeviceSupport;         	// For Generic Device Support
using Crestron.SimplSharpPro.UI;
using Crestron.SimplSharp.CrestronIO;

namespace TP_Events
{
    public partial class ControlSystem : CrestronControlSystem
    {
        private XpanelForSmartGraphics tp;
        private Controls01 controls01 = new Controls01();
        private Controls02 controls02 = new Controls02();
        
        public ControlSystem()
            : base()
        {
            try
            {
                Thread.MaxNumberOfUserThreads = 20;
                if (this.SupportsEthernet)
                {
                    tp = new XpanelForSmartGraphics(0x71, this);
                    if (tp.Register() != eDeviceRegistrationUnRegistrationResponse.Success)
                        ErrorLog.Error("Panel {0} did not register: {1}", tp.ID, tp.RegistrationFailureReason);
                }
            }
            catch (Exception e)
            {
                ErrorLog.Error("Error in the constructor: {0}", e.Message);
            }
        }

        public override void InitializeSystem()
        {
            try
            {
                if (tp.Registered)
                {
                    String SGDpath = string.Format("{0}\\User\\TP_Events.sgd", Directory.GetApplicationRootDirectory());
                    tp.LoadSmartObjects(SGDpath);
                    CrestronConsole.PrintLine("loaded {0} smartobjects", tp.SmartObjects.Count);
                }
                tp.SigChange += new SigEventHandler(tp_SigChange);
                InitializeSignals();
                controls01.Initialize(tp);
                controls02.Initialize(tp, tp.SmartObjects[20]);         // SmartObject ID is 20
                
            }
            catch (Exception e)
            {
                ErrorLog.Error("Error in InitializeSystem: {0}", e.Message);
            }
        }

        void tp_SigChange(BasicTriList currentDevice, SigEventArgs args)
        {
            //CrestronConsole.PrintLine("Event Type: {0}, Signal: {1}", args.Sig.Type, args.Sig.Name);
            switch (args.Sig.Type)
            {
                case eSigType.Bool:
                    if (digitals.ContainsKey(args.Sig.Number)) digitals[args.Sig.Number](currentDevice, args);
                    break;
                case eSigType.UShort:
                    if (analogs.ContainsKey(args.Sig.Number)) analogs[args.Sig.Number](currentDevice, args);
                    break;
                case eSigType.String:
                    if (serials.ContainsKey(args.Sig.Number)) serials[args.Sig.Number](currentDevice, args);
                    break;
            }
        }
    }

}











