﻿using System;
using System.Collections.Generic;
using Crestron.SimplSharp;                          	// For Basic SIMPL# Classes
using Crestron.SimplSharpPro;                       	// For Basic SIMPL#Pro classes
using Crestron.SimplSharpPro.DeviceSupport;         	// For Generic Device Support
using Crestron.SimplSharpPro.UI;

namespace TP_Events
{
    public class Controls01
    {
        enum Joins
        {
            dig111 = 111,
            dig112 = 112,
            dig113 = 113,
            dig114 = 114,
            ana111 = 111,
            ser111 = 111,
        };

        private Dictionary<uint, SigEventHandler> digitals;
        private Dictionary<uint, SigEventHandler> analogs;
        private Dictionary<uint, SigEventHandler> serials;

        private BasicTriList tp;

        public Controls01()
        {

            #region JOIN_DICTIONARY
            digitals = new Dictionary<uint, SigEventHandler>()
            {
                {(uint)Joins.dig111, dig111},
                {(uint)Joins.dig112, dig112},
                {(uint)Joins.dig113, dig113},
                {(uint)Joins.dig114, dig114},
            };
            analogs = new Dictionary<uint, SigEventHandler>()
            {
                {(uint)Joins.ana111, ana111},
            };
            serials = new Dictionary<uint, SigEventHandler>()
            {
                {(uint)Joins.ser111, ser111},
            };
            #endregion
        }

        public void Initialize(BasicTriList tp)
        {
            this.tp = tp;
            tp.SigChange += new SigEventHandler(tp_SigChange);          
        }

        void tp_SigChange(BasicTriList currentDevice, SigEventArgs args)
        {
            switch (args.Sig.Type)
            {
                case eSigType.Bool:
                    if (digitals.ContainsKey(args.Sig.Number)) digitals[args.Sig.Number](currentDevice, args);
                    break;
                case eSigType.UShort:
                    if (analogs.ContainsKey(args.Sig.Number)) analogs[args.Sig.Number](currentDevice, args);
                    break;
                case eSigType.String:
                    if (serials.ContainsKey(args.Sig.Number)) serials[args.Sig.Number](currentDevice, args);
                    break;
            }           
        }

        private void dig111(BasicTriList currentDevice, SigEventArgs args)
        {
            if (args.Sig.BoolValue)
                CrestronConsole.PrintLine("{0}: pushed", args.Sig.Name);
            else
                CrestronConsole.PrintLine("{0}: released", args.Sig.Name);
        }
        private void dig112(BasicTriList currentDevice, SigEventArgs args)
        {
            if (args.Sig.BoolValue)
                CrestronConsole.PrintLine("{0}: pushed", args.Sig.Name);
            else
                CrestronConsole.PrintLine("{0}: released", args.Sig.Name);
        }
        private void dig113(BasicTriList currentDevice, SigEventArgs args)
        {
            if (args.Sig.BoolValue)
                CrestronConsole.PrintLine("{0}: pushed", args.Sig.Name);
            else
                CrestronConsole.PrintLine("{0}: released", args.Sig.Name);
        }
        private void dig114(BasicTriList currentDevice, SigEventArgs args)
        {
            if (args.Sig.BoolValue)
                CrestronConsole.PrintLine("{0}: pushed", args.Sig.Name);
            else
                CrestronConsole.PrintLine("{0}: released", args.Sig.Name);
        }
        private void ana111(BasicTriList currentDevice, SigEventArgs args)
        {
            CrestronConsole.PrintLine("{0}: {1}", args.Sig.Name, args.Sig.UShortValue);
            currentDevice.UShortInput[args.Sig.Number].UShortValue = args.Sig.UShortValue;
        }
        private void ser111(BasicTriList currentDevice, SigEventArgs args)
        {
            CrestronConsole.PrintLine("{0}: {1}", args.Sig.Name, args.Sig.StringValue);
        }
    }
}