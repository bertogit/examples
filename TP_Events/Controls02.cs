﻿using System;
using System.Collections.Generic;
using Crestron.SimplSharp;                          	// For Basic SIMPL# Classes
using Crestron.SimplSharpPro;                       	// For Basic SIMPL#Pro classes
using Crestron.SimplSharpPro.DeviceSupport;         	// For Generic Device Support
using Crestron.SimplSharpPro.UI;

namespace TP_Events
{
    public class Controls02
    {
        enum Joins
        {
            dig121 = 121,
            dig122 = 122,
            dig123 = 123,
            dig124 = 124,
            Backspace = 125,
            ana121 = 121,
            ser121 = 121,
            KeypadStringInput = 122,
            KeypadStringOutput = 123
        };

        private Dictionary<uint, SigEventHandler> digitals;
        private Dictionary<uint, SigEventHandler> analogs;
        private Dictionary<uint, SigEventHandler> serials;

        private BasicTriList tp;
        private SmartObject keypadSmartObject;
        private ButtonHeld hold = new ButtonHeld();
        public string keypadString = "";

        public Controls02()
        {

            #region JOIN_DICTIONARY
            digitals = new Dictionary<uint, SigEventHandler>()
            {
                {(uint)Joins.dig121, dig121},
                {(uint)Joins.dig122, dig122},
                {(uint)Joins.dig123, dig123},
                {(uint)Joins.dig124, dig124},
                {(uint)Joins.Backspace, Backspace},      // Keypad Backspace
            };
            analogs = new Dictionary<uint, SigEventHandler>()
            {
                {(uint)Joins.ana121, ana121},
            };
            serials = new Dictionary<uint, SigEventHandler>()
            {
                {(uint)Joins.ser121, ser121},
                {(uint)Joins.KeypadStringInput, KeypadStringInput},      // Keypad StringInput
                {(uint)Joins.KeypadStringOutput, KeypadStringOutput},      // Keypad StringOutput
            };
            #endregion
        }

        public void Initialize(BasicTriList tp, SmartObject keypadSmartObject)
        {
            this.tp = tp;
            tp.SigChange += new SigEventHandler(tp_SigChange);
            this.keypadSmartObject = keypadSmartObject;
            keypadSmartObject.SigChange += new SmartObjectSigChangeEventHandler(keypad_SigChange);
        }

        void tp_SigChange(BasicTriList currentDevice, SigEventArgs args)
        {
            switch (args.Sig.Type)
            {
                case eSigType.Bool:
                    if (digitals.ContainsKey(args.Sig.Number)) digitals[args.Sig.Number](currentDevice, args);
                    break;
                case eSigType.UShort:
                    if (analogs.ContainsKey(args.Sig.Number)) analogs[args.Sig.Number](currentDevice, args);
                    break;
                case eSigType.String:
                    if (serials.ContainsKey(args.Sig.Number)) serials[args.Sig.Number](currentDevice, args);
                    break;
            }
        }

        void keypad_SigChange(GenericBase currentDevice, SmartObjectEventArgs args)
        {
            //CrestronConsole.PrintLine("SmartObject {0}: Event Type: {1}, Signal: {2}", args.SmartObjectArgs.ID, args.Sig.Type, args.Sig.Name );
            switch (args.Sig.Type)
            {
                case eSigType.Bool:
                    if (args.Sig.BoolValue == true)
                    {
                        CrestronConsole.PrintLine("keypad button = {0}", args.Sig.Name);
                        switch (args.Sig.Name)
                        {
                            case "0": keypadString += "0"; break;
                            case "1": keypadString += "1"; break;
                            case "2": keypadString += "2"; break;
                            case "3": keypadString += "3"; break;
                            case "4": keypadString += "4"; break;
                            case "5": keypadString += "5"; break;
                            case "6": keypadString += "6"; break;
                            case "7": keypadString += "7"; break;
                            case "8": keypadString += "8"; break;
                            case "9": keypadString += "9"; break;
                            case "Misc_1": keypadString += "*"; break;
                            case "Misc_2": keypadString += "#"; break;
                        }
                        tp.StringInput[(uint)Joins.KeypadStringInput].StringValue = keypadString;
                    }
                    break;
            }
        }

        private void dig121(BasicTriList currentDevice, SigEventArgs args)
        {
            if (args.Sig.BoolValue)
                CrestronConsole.PrintLine("{0}: pushed", args.Sig.Name);
            else
                CrestronConsole.PrintLine("{0}: released", args.Sig.Name);
        }
        private void dig122(BasicTriList currentDevice, SigEventArgs args)
        {
            if (args.Sig.BoolValue)
                CrestronConsole.PrintLine("{0}: pushed", args.Sig.Name);
            else
                CrestronConsole.PrintLine("{0}: released", args.Sig.Name);
        }
        private void dig123(BasicTriList currentDevice, SigEventArgs args)
        {
            if (args.Sig.BoolValue)
                CrestronConsole.PrintLine("{0}: pushed", args.Sig.Name);
            else
                CrestronConsole.PrintLine("{0}: released", args.Sig.Name);
        }
        private void dig124(BasicTriList currentDevice, SigEventArgs args)
        {
            if (args.Sig.BoolValue)
                CrestronConsole.PrintLine("{0}: pushed", args.Sig.Name);
            else
                CrestronConsole.PrintLine("{0}: released", args.Sig.Name);
        }
        private void Backspace(BasicTriList currentDevice, SigEventArgs args)
        {
            if (args.Sig.BoolValue && keypadString.Length > 0)
            {
                keypadString = keypadString.Substring(0, keypadString.Length - 1);
                tp.StringInput[(uint)Joins.KeypadStringInput].StringValue = keypadString;
                hold.Start(2000, 100, currentDevice, args);
                hold.HeldEvent += new SigEventHandler(BackspaceHeld);
            }
            else if (!args.Sig.BoolValue)
            {
                hold.Stop();
                hold.HeldEvent -= new SigEventHandler(BackspaceHeld);
            }
        }
        private void BackspaceHeld(BasicTriList currentDevice, SigEventArgs args)
        {
            CrestronConsole.PrintLine("{0}: held", args.Sig.Name);
            if (keypadString.Length > 0)
            {
                keypadString = keypadString.Substring(0, keypadString.Length - 1);
                tp.StringInput[(uint)Joins.KeypadStringInput].StringValue = keypadString;
            }
        }
        private void ana121(BasicTriList currentDevice, SigEventArgs args)
        {
            CrestronConsole.PrintLine("{0}: {1}", args.Sig.Name, args.Sig.UShortValue);
            currentDevice.UShortInput[args.Sig.Number].UShortValue = args.Sig.UShortValue;
        }
        private void ser121(BasicTriList currentDevice, SigEventArgs args)
        {
            CrestronConsole.PrintLine("{0}: {1}", args.Sig.Name, args.Sig.StringValue);
        }
        private void KeypadStringInput(BasicTriList currentDevice, SigEventArgs args)       // not used
        {
            CrestronConsole.PrintLine("{0}: {1}", args.Sig.Name, args.Sig.StringValue);
        }
        private void KeypadStringOutput(BasicTriList currentDevice, SigEventArgs args)
        {
            CrestronConsole.PrintLine("{0}: {1}", args.Sig.Name, args.Sig.StringValue);
            if (args.Sig.StringValue.ToString().Length > 0)
            {
                keypadString = args.Sig.StringValue;
                tp.StringInput[(uint)Joins.KeypadStringInput].StringValue = keypadString;
            }

        }
    }
}