﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDoList.Models;

namespace ToDoList.Controllers
{
    public class MainController : Controller
    {
        ToDoListRepo _ToDoListRepo = new ToDoListRepo();

        public ActionResult Index()
        {
            return View(_ToDoListRepo.getItems());
        }

        [HttpGet]
        public ActionResult AddItem()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddItem(ToDoItem item)
        {
            _ToDoListRepo.AddItem(item);
            return RedirectToAction("../Main/Index");
        }

        [HttpGet]                                     
        public ActionResult Delete(string name)
        {
            return View(_ToDoListRepo.getItem(name));
        }

        [HttpPost]                                      //[HttpPost, ActionName("Delete")]
        public ActionResult Delete(ToDoItem item)
        {
            _ToDoListRepo.Delete(item);
            return RedirectToAction("../Main/Index");
        }

        [HttpGet]
        public ActionResult Update(string name)
        {
            TempData["oldName"] = name;
            return View(_ToDoListRepo.getItem(name));
        }

        [HttpPost]                                      
        public ActionResult Update(ToDoItem item)
        {           
            _ToDoListRepo.Update(item,TempData["oldName"].ToString());
            return RedirectToAction("../Main/Index");
        }

        [HttpGet]
        public ActionResult Info(string name)
        {
            return View(_ToDoListRepo.getItem(name));
        }

        [HttpPost]
        public ActionResult CheckBox(string name, string isChecked)
        {
            _ToDoListRepo.CheckBox(name, isChecked == "true");
            return RedirectToAction("../Main/Index");
        }

    }
}