﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoList.Models
{
    public class ToDoItem
    {
        public bool done { get; set; }
        public DateTime date { get; set; }
        public string name { get; set; }
        public string details { get; set; }

        public ToDoItem()
        {
            done = false;
            date = DateTime.Now;
            name = "";
            details = "";
        }

        public ToDoItem(bool done, DateTime date, string name, string details)
        {
            this.done = done;
            this.date = date;
            this.name = name;
            this.details = details;
        }
    }
}