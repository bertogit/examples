﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Web;

namespace ToDoList.Models
{
    public class ToDoListRepo
    {
        private List<ToDoItem> Items;
        private XDocument ToDoListData;

        public ToDoListRepo()
        {
            ToDoListData = XDocument.Load(HttpContext.Current.Server.MapPath("~/App_Data/ToDoListData.xml"));
            Items = new List<ToDoItem>();
        }

        public List<ToDoItem> getItems()
        {
            try
            {
                var xitems = ToDoListData.Descendants("item")
                                .Select(i => new ToDoItem((bool)i.Element("done"),(DateTime)i.Element("date"),i.Element("name").Value, i.Element("details").Value));

                Items.AddRange(xitems.ToList());
                return Items;
            }
            catch
            {
                return Items;
            }
        }

        public ToDoItem getItem(string name)
        {
            var xitem = ToDoListData.Descendants("item")
               .Elements("name")
               .Where(x => x.Value == name)
               .Select(x => x.Parent).FirstOrDefault();

            ToDoItem item = new ToDoItem((bool)xitem.Element("done"), (DateTime)xitem.Element("date"), xitem.Element("name").Value, xitem.Element("details").Value);
            return item;
        }

        public void AddItem(ToDoItem item)
        {
            ToDoListData.Root.Add(new XElement("item",
                new XElement("done", item.done),
                new XElement("date", item.date),
                new XElement("name", item.name),
                new XElement("details", item.details)
                ));

            ToDoListData.Save(HttpContext.Current.Server.MapPath("~/App_Data/ToDoListData.xml"));
        }

        public void Update(ToDoItem item, string Name)
        {
            var xitem = ToDoListData.Descendants("item")
           .Elements("name")
           .Where(x => x.Value == Name)
           .Select(x => x.Parent).FirstOrDefault();

            xitem.SetElementValue("done", item.done);
            xitem.SetElementValue("date", item.date);
            xitem.SetElementValue("name", item.name);
            xitem.SetElementValue("details", item.details);
            ToDoListData.Save(HttpContext.Current.Server.MapPath("~/App_Data/ToDoListData.xml"));
        }

        public void Delete(ToDoItem item)
        {
            ToDoListData.Descendants("item")
               .Elements("name")
               .Where(x => x.Value == item.name)
               .Select(x => x.Parent)
               .Remove();

            ToDoListData.Save(HttpContext.Current.Server.MapPath("~/App_Data/ToDoListData.xml"));
        }

        public void CheckBox(string name, bool isChecked)
        {
            ToDoItem item = getItem(name);
            item.done = isChecked;
            Update(item, item.name);
        }
 
    }
}