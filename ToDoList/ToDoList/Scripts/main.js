﻿
$(document).ready(function () {

});

$('#datepicker').datepicker();

$('.checkBox').change(function () {

    isChecked = $(this).is(':checked');

    $.ajax({
        url: $(this).data('url'),
        type: 'POST',
        data: { isChecked:isChecked } ,
        success: function (result) {
            //console.log(isChecked);    
        }
    });
});

$('.itemDate').each(function () {
    var itemDate = $(this).text();
    var today = new Date();
    if (Date.parse(itemDate) < Date.parse(today))
    {
        //console.log("expired:" + $(this).text());
        $(this).toggleClass("highlight");
    }
});

function sortTable(n, type) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    var xval, yval;
    table = document.getElementById("ToDoTable");
    switching = true;
    dir = "asc";
    while (switching) {
        switching = false;
        rows = table.getElementsByTagName("TR");
        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];

            switch (type)
            {
                case 'checkbox':
                    xval = x.children[0].checked;
                    yval = y.children[0].checked;
                    break;
                case 'date':
                    xval = Date.parse(x.innerHTML)
                    yval = Date.parse(y.innerHTML)
                    break;
                default:
                    xval = x.innerHTML.toLowerCase()
                    yval = y.innerHTML.toLowerCase()
                    break;                  
            }

            if (dir == "asc") {
                if (xval > yval) {
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
                if (xval < yval) {
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}


